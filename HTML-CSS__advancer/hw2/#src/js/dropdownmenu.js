'use strict'

const menu = document.querySelector('.icon__menu');
const menulist= document.querySelector('.header__list');
menu.addEventListener('click', () => {
    menu.classList.toggle('active');
    menulist.classList.toggle('active');
})
